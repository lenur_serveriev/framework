<?php

use Framework\Application;
use Symfony\Component\HttpFoundation\Request;

require_once __DIR__ . '/../vendor/autoload.php';

define('ROOT_DIR', __DIR__ . '/../');

$request = Request::createFromGlobals();

$application = new Application(ROOT_DIR);
$application
    ->bootstrap()
    ->handle($request)
    ->send();
