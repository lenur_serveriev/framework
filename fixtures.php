<?php

use Framework\Application;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManager;
use Entity\User;
use Doctrine\ORM\Tools\SchemaTool;

require_once __DIR__ . '/vendor/autoload.php';

define('ROOT_DIR', __DIR__);

$request = Request::createFromGlobals();

$application = new Application(ROOT_DIR);
$application->bootstrap();


/** @var EntityManager $entityManager */
$entityManager = $application->getContainer()->get('doctrine.entity_manager');
$connection = $entityManager->getConnection();

//not working
//if (!in_array($connection->getDatabase(), $connection->getSchemaManager()->listDatabases())) {
//    $connection->getSchemaManager()->createDatabase($connection->getDatabase());
//}

$pdoConn = new PDO(sprintf("%s:host%",
    str_replace("pdo_", "", $connection->getDriver()->getName()),
    $connection->getHost()),
    $connection->getUsername(),
    $connection->getPassword()
);
$pdoConn->query(sprintf("CREATE DATABASE IF NOT EXISTS %s", $connection->getDatabase()));

$schemaTool = new SchemaTool($entityManager);

$classes = $entityManager->getMetadataFactory()->getAllMetadata();
$schemaTool->updateSchema($classes);

$user = new User();
$user->setUsername('test');
$user->setPassword('password');
$user->setToken(md5(uniqid()));

$entityManager->persist($user);
$entityManager->flush();