<?php

namespace Framework\Serializer;

use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class SerializerFactory
{
    public static function createSerializer()
    {
        return new Serializer([new ObjectNormalizer()], [new JsonEncoder()]);
    }
}