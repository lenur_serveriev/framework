<?php
namespace Framework;

use Entity\User;
use Framework\Secure\SecureExtension;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel;
use Symfony\Component\HttpKernel\Controller\ArgumentResolver;
use Symfony\Component\HttpKernel\Controller\ControllerResolver;
use Symfony\Component\Routing\Loader\YamlFileLoader AS RoutingYamlFileLoader;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\Matcher\UrlMatcher;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Framework\Doctrine\DoctrineExtension;
use Symfony\Component\Security\Core\Authentication\Token\AnonymousToken;
use Symfony\Component\HttpFoundation\File\Exception\AccessDeniedException;

/**
 * Class Application
 * @package Framework
 */
class Application
{
    /** @var string */
    protected $rootDir;
    /** @var UrlMatcher */
    protected $matcher;
    /** @var ContainerBuilder */
    protected $container;

    public function __construct(string $rootDir)
    {
        $this->rootDir = $rootDir;
    }

    public function bootstrap() : self
    {
        //load services
        $locator = new FileLocator($this->rootDir . '/app/config');

        $this->container = new ContainerBuilder();

        $loader = new YamlFileLoader($this->container, $locator);
        $loader->load('parameters.yml');
        $loader->load('services.yml');

        //load routes
        $loader = new RoutingYamlFileLoader($locator);
        $routes = $loader->load('routing.yml');

        $this->matcher = new UrlMatcher($routes, new RequestContext());

        $this->loadExtensions();

        return $this;
    }

    /**
     * @inheritdoc
     */
    private function loadExtensions(): void
    {
        $parameterBag = $this->container->getParameterBag();
        $parameterBag->add($this->getDefaultParameters());

        $doctrineExtension = new DoctrineExtension();
        $this->container->registerExtension($doctrineExtension);
        $doctrineExtension->load([], $this->container);

        $secureExtension = new SecureExtension();
        $this->container->registerExtension($secureExtension);
        $secureExtension->load([], $this->container);

        $parameterBag->add([
            'doctrine' => $this->container->getExtensionConfig('doctrine')[0],
            'secure' => $this->container->getExtensionConfig('secure')[0]
        ]);
    }

    /**
     * @param Request $request
     * @return mixed|Response
     */
    public function handle(Request $request) : Response
    {
        $controllerResolver = new ControllerResolver();
        $argumentResolver = new ArgumentResolver();

        $this->matcher->getContext()->fromRequest($request);

        $this->container->set('request', $request);

        $this->container
            ->get('security.token_creator')
            ->createToken();

        try {
            $request->attributes->add($this->matcher->match($request->getPathInfo()));
            $controller = $controllerResolver->getController($request);
            $arguments = $argumentResolver->getArguments($request, $controller);

            if ($controller[0] instanceof Controller) {
                $controller[0]->setContainer($this->container);
            }

            return call_user_func_array($controller, $arguments);
        } catch (ResourceNotFoundException $e) {
            return new Response('Not found', Response::HTTP_NOT_FOUND);
        } catch (AccessDeniedException $e) {
            return new Response('Access denied', Response::HTTP_FORBIDDEN);
        } catch (\Exception $e) {
            return new Response('An error occurred', Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @return array
     */
    private function getDefaultParameters() : array
    {
        $defaults = [
            'kernel.root_dir' => $this->rootDir,
            'kernel.config_dir' => $this->rootDir . "/app/config"
        ];

        return $defaults;
    }

    /**
     * @return ContainerBuilder
     */
    public function getContainer(): ContainerBuilder
    {
        return $this->container;
    }
}