<?php

namespace Framework\Secure\Authentication;

interface AuthenticationInterface
{
    public function showLoginForm();

    public function authenticate();
}
