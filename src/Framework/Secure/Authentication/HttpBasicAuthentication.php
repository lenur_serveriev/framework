<?php

namespace Framework\Secure\Authentication;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;

class HttpBasicAuthentication implements AuthenticationInterface
{
    /** @var Request */
    private $request;

    public function __construct(ContainerInterface $container)
    {
        $this->request = $container->get('request');
    }

    public function showLoginForm(): Response
    {
        $response = new Response();
        $response->headers->set('WWW-Authenticate', 'Basic realm="framework"');
        $response->setStatusCode(Response::HTTP_UNAUTHORIZED);

        return $response;
    }

    public function authenticate()
    {
    }
}
