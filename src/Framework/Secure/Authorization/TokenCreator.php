<?php

namespace Framework\Secure\Authorization;

use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Security\Core\Authentication\Token\AnonymousToken;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

use Entity\User;

/**
 * Class TokenCreator
 * @package Framework\Secure\Authorization
 */
class TokenCreator
{
    /** @var ContainerInterface */
    private $container;
    /** @var Request */
    private $request;
    /** @var EntityManager */
    private $entityManager;
    private $config;

    public function __construct(ContainerInterface $container, array $secureConfiguration)
    {
        $this->container = $container;
        $this->request = $container->get('request');
        $this->entityManager = $this->container->get('doctrine.entity_manager');
        $this->config = $secureConfiguration;
    }

    /**
     * @inheritdoc
     */
    public function createToken(): void
    {
        if (null === $this->container->get('security.token_storage')->getToken()) {
            
            $authorizationToken = $this->request->headers->get('Authorization');

            if (null === $authorizationToken) {
                $this->createAnonymousToken();
            } else {
                /** @var User $user */
                $user = $this->entityManager
                    ->getRepository('Entity\User')
                    ->loadUserByToken($authorizationToken);

                if ($user) {
                    $this->createUsernamePasswordToken($user);
                } else {
                    $this->createAnonymousToken();
                }
            }
        }
    }

    /**
     * @param User $user
     */
    private function createUsernamePasswordToken(User $user): void
    {
        $token = new UsernamePasswordToken($user, null, $this->config['provider_key'], $user->getRoles());
        $this->container->get('security.token_storage')->setToken($token);
        $this->container->get('session')->set('framework', serialize($token));
    }

    /**
     * @inheritdoc
     */
    private function createAnonymousToken(): void
    {
        $user = new User();
        $user->setUsername('anonymous');
        $user->setPassword('password');

        $token = new AnonymousToken($this->config['secret'], $user);
        $this->container->get('security.token_storage')->setToken($token);
        $this->container->get('session')->set('framework', serialize($token));
    }
}
