<?php

namespace Framework\Doctrine;

use Doctrine\Common\Proxy\AbstractProxyFactory;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\Setup;

class EntityManagerFactory
{
    /**
     * EntityManager factory method
     *
     * @param array $doctrineConfiguration
     * @return EntityManager
     */
    public static function createEntityManager(array $doctrineConfiguration)
    {
        $paths = $doctrineConfiguration['paths'];

        $config = Setup::createAnnotationMetadataConfiguration($paths, true, null, null, false);
        $config->setAutoGenerateProxyClasses(AbstractProxyFactory::AUTOGENERATE_FILE_NOT_EXISTS);

        return EntityManager::create($doctrineConfiguration['connection'], $config);
    }
}
