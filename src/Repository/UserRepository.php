<?php

namespace Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Class UserRepository
 * @package Repository
 */
class UserRepository extends EntityRepository
{
    /**
     * @param string $token
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function loadUserByToken(string $token)
    {
        return $this->createQueryBuilder('u')
            ->where('u.token = :token')
            ->setParameter('token', $token)
            ->getQuery()
            ->getOneOrNullResult();
    }
}
