<?php

namespace Controller;

use Framework\Controller;
use Symfony\Component\HttpFoundation\File\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class UserController
 * @package Controller
 */
class UserController extends Controller
{
    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function usersAction(Request $request)
    {
        if ($this->isGranted('ROLE_USER')) {
            $users = $this->entityManager()->getRepository('Entity\User')->findAll();

            return $this->json($users);
        } else {
            throw new AccessDeniedException('');
        }
    }
}
